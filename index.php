<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Repetition Control Structures and Array Manipulation ACTIVITY</title>
</head>
<body>
    <h1>Activity Solution</h1>

    <h2>Divisibles by Five</h2>
    <?php fiveDivisible(); ?>

    <h2>Array Manipulation</h2>

    <!-- ● Accept a name of the student and add it to the student array. -->
    <!-- ● Print the names added so far in the student array. -->
    <?php array_push($students, 'John Smith'); ?>
    <pre><?php var_dump($students); ?></pre>
    <pre><?= count($students); ?></pre>

    <!-- ● Add another student then print the array and its new count. -->
    <?php array_push($students, 'Jane Smith'); ?>
    <pre><?php var_dump($students); ?></pre>
    <pre><?= count($students); ?></pre>

    <!-- ● Finally, remove the first student and print the array and its count. -->
    <?php array_shift($students);?>
    <pre><?php var_dump($students); ?></pre>
    <pre><?= count($students); ?></pre>


</body>
</html>