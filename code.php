<?php

/*
Loops
● Using loops, print all numbers that are divisible by 5.
● Stop the loop when the loop reaches its 1000th iteration.
*/

function fiveDivisible() {
    for($count = 0; $count <= 1000; $count++) {
        if($count % 5 === 0) {
            echo $count.', ';
        } else {
            continue;
        }
    }
}

// Arrays
// ● Create an empty array named “students”
$students = [];

